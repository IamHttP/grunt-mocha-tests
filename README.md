Unit testing grunt-mocha with requirejs.


to install : 

1. git clone the repo,
2. npm install
3. run 'grunt' from the CLI in the project root to run the tests.

As a proof of concept, the tests implemented are against a backbone model and some jquery dom manipulation.