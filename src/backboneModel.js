/**
 * Created by patrik.tolosa on 17/08/2016
 */
(function () {

    function module(backbone) {
        return backbone.Model.extend({});
    }

    if (typeof define == 'function') {
        define(['backbone'], module);
    }
    else {
        //window.module = module;
    }
})();
