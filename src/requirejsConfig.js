require.config({
    baseUrl: "../src",
    paths: {
        "backbone"   : "https://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.3.3/backbone-min",
        "underscore" : "https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min",
        "jquery"     : "https://code.jquery.com/jquery-2.2.4.min",
        'dotJS'      : "https://cdnjs.cloudflare.com/ajax/libs/dot/1.0.3/doT",
        'text'       : "https://cdnjs.cloudflare.com/ajax/libs/require-text/2.0.10/text",
    },
});