// Gruntfile.js
module.exports = function(grunt){








    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        mocha : {
            all : {
                src : ['tests/testrunner.html']
            },
            options : {
                run: true,
                page: {
                    settings: {
                        webSecurityEnabled: false,  // disable cors checks in phantomjs
                    },
                },
            },
        },
        jshint : {
            allFiles : ['src/**/*.js'],
            options: {
                jshintrc: '.jshintrc'
            }
        },
        concat : {
            options: {
                seperator: ';'
            },
            dist : {
                src :['src/**/*.js'],
                dest : 'dist/build.js'
            }
        },
        uglify : {
            min: {
                files: {
                    'dist/build.min.js': ['dist/build.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-mocha');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['jshint','concat','mocha','uglify']);
};