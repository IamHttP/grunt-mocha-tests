describe('testJqueryAjax', function(){

    it('fetch data from an xhr', function(done){
        this.timeout(3000);

        require(['jquery'],function($){

            $.ajax({
                url: 'https://api.github.com',
            }).done( function(data){
                try{
                    chai.assert.equal( true , 'authorizations_url' in data );
                    done();
                }
                catch(e){
                    done(e);
                }
            }).fail(function(){
                try{
                    chai.assert.ok( false );
                    done();
                }
                catch(e){
                    done(e);
                }
            });

        });
    });
});
