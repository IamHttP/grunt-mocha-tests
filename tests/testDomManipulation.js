/**
 *
 * Desc : This file showcases testing dom manipulation with jquery.
 * Special note should be given to the afterEach function which cleans the state of the dom so tests are atomic
 *
 */

describe('Test dom manipulation', function(){

    //KEEP IT ATOMIC - STATE RESET
    afterEach(function(){
        require(['jquery'],function($){
            var $root = $('#atomicTests').empty();
        });
    });

    it('Should add a div to the dom', function(done){
        require(['jquery'],function($){
            try{
                var $root = $('#atomicTests');
                $root.append('<div></div>');
                chai.assert.equal( 1 , $root.find('div').length );
                done();
            }
            catch(e){
                done(e);
            }
        });
    });
    it('Should add a div to the dom', function(done){
        require(['jquery'],function($){
            try{
                var $root = $('#atomicTests');
                $root.append('<div></div>');
                chai.assert.equal( 1 , $root.find('div').length );
                done();
            }
            catch(e){
                done(e);
            }
        });
    });

    it('Should add a div to the dom', function(done){
        require(['jquery'],function($){
            try{
                var $root = $('#atomicTests');
                $root.append('<div></div>');
                chai.assert.equal( 1 , $root.find('div').length );
                done();
            }
            catch(e){
                done(e);
            }
        });
    });
});
