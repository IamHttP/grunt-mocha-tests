/**
 * Created by patrik.tolosa on 17/08/2016.
 */
describe('Array', function(){
    describe('Base test', function(){
        it('Should notify is calc is undefined', function(done){
            require(['backboneModel'],function(backboneModel){
                try{
                    var model = new backboneModel();
                    model.set('username','patrick');
                    chai.assert.equal('patrick',model.get('username'));

                    chai.assert.equal( 'patrick' , model.get('username') );
                    done();
                }
                catch(e){
                    done(e);
                }
            });
        });
    });
});